package com.bitsmi.yggdrasil.code.annotation.processors.tests;

import java.time.LocalDateTime;

import com.bitsmi.yggdrasil.code.annotation.processors.BuilderGen;
import com.bitsmi.yggdrasil.code.annotation.processors.BuilderProperty;

@BuilderGen("CustomEntityBuilder")
public class NamedBuilderTestEntity 
{
	@BuilderProperty
	private Long id;
	
	@BuilderProperty
	private int primitive;
	
	@BuilderProperty
	private LocalDateTime date;
	
	private String transientProperty;
	
	private String computedProperty;
	
	public Long getId() 
	{
		return id;
	}

	public void setId(Long id) 
	{
		this.id = id;
	}
	
	public int getPrimitive() 
	{
		return primitive;
	}

	public void setPrimitive(int primitive) 
	{
		this.primitive = primitive;
	}

	public LocalDateTime getDate() 
	{
		return date;
	}

	public void setDate(LocalDateTime date) 
	{
		this.date = date;
	}

	public String getTransientProperty() 
	{
		return transientProperty;
	}

	public void setTransientProperty(String transientProperty) 
	{
		this.transientProperty = transientProperty;
	}

	public String getComputedProperty() 
	{
		return computedProperty;
	}

	@BuilderProperty
	public void setComputedProperty(String property)
	{
		this.computedProperty = property;
	}
}
