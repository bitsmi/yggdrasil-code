package com.bitsmi.yggdrasil.code.annotation.processors.tests;

import java.util.List;

import com.bitsmi.yggdrasil.code.annotation.processors.BuilderGen;
import com.bitsmi.yggdrasil.code.annotation.processors.BuilderProperty;

@BuilderGen
public class GenericsWildcardTestEntity
{
    @BuilderProperty
    private List<? super Exception> errors;

    public List<? super Exception> getErrors()
    {
        return errors;
    }

    public void setErrors(List<? super Exception> errors)
    {
        this.errors = errors;
    }
}
