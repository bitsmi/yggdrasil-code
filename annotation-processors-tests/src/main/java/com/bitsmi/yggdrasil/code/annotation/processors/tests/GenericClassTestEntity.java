package com.bitsmi.yggdrasil.code.annotation.processors.tests;

import java.util.function.Function;

import com.bitsmi.yggdrasil.code.annotation.processors.BuilderGen;
import com.bitsmi.yggdrasil.code.annotation.processors.BuilderProperty;
import com.bitsmi.yggdrasil.commons.util.ITransformable;
import com.bitsmi.yggdrasil.commons.util.ITransformer;

@BuilderGen
public class GenericClassTestEntity implements ITransformable
{
    @BuilderProperty
    private Long id;
    
    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public <R> R doDummyStuff(R dummyValue)
    {
        return dummyValue;
    }

    @Override
    public ITransformer<GenericClassTestEntity> transformed()
    {
        return this::transform;
    }
    
    private <R> R transform(Function<? super GenericClassTestEntity, ? extends R> f) 
    {
        return f.apply(this);
    }
}
