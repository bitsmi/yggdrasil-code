package com.bitsmi.yggdrasil.code.annotation.processors.tests;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.bitsmi.yggdrasil.code.annotation.processors.BuilderProperty;
import com.bitsmi.yggdrasil.code.annotation.processors.BuilderGen;

@BuilderGen
public class ValidatedTestEntity 
{
	@BuilderProperty
	@NotNull(message = "ID cannot be null")
    private Long id;
 
	@BuilderProperty
    @AssertTrue
    private boolean enabled;
 
	@BuilderProperty
    @Size(min = 10, max = 100, message = "Value must be between 10 and 100 characters")
	@NotNull
    private String property;
 
	@BuilderProperty
    @Min(value = 10, message = "Value should not be less than 10")
    @Max(value = 150, message = "value should not be greater than 150")
    private int primitive;

	public Long getId() 
	{
		return id;
	}

	public void setId(Long id) 
	{
		this.id = id;
	}

	public boolean isEnabled() 
	{
		return enabled;
	}

	public void setEnabled(boolean enabled) 
	{
		this.enabled = enabled;
	}

	public String getProperty() 
	{
		return property;
	}

	public void setProperty(String property) 
	{
		this.property = property;
	}

	public int getPrimitive() 
	{
		return primitive;
	}

	public void setPrimitive(int primitive) 
	{
		this.primitive = primitive;
	}
}
