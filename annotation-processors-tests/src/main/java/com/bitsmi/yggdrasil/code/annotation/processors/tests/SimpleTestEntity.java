package com.bitsmi.yggdrasil.code.annotation.processors.tests;

import java.time.LocalDateTime;
import java.util.Map;

import org.apache.commons.lang3.mutable.MutableInt;

import com.bitsmi.yggdrasil.code.annotation.processors.BuilderGen;
import com.bitsmi.yggdrasil.code.annotation.processors.BuilderProperty;

@BuilderGen
public class SimpleTestEntity 
{
	@BuilderProperty
	private Long id;
	
	@BuilderProperty
	private int primitive;
	
	@BuilderProperty(defaultValue = "LocalDateTime.of(2021, 01, 01, 12, 00)")
	private LocalDateTime date;
	
	@BuilderProperty
	private Map<MutableInt, LocalDateTime> genericsProperty;
	
	private String transientProperty;
	
	private String computedProperty;
	
	@BuilderProperty(defaultValue = "'String default property'")
	private String defaultStringProperty;
	@BuilderProperty(defaultValue = "String default property 2")
	private String defaultStringProperty2;
	@BuilderProperty(defaultValue = "3L")
	private long defaultLongProperty;
	@BuilderProperty(defaultValue = "c")
	private char defaultCharProperty;
	
	public Long getId() 
	{
		return id;
	}

	public void setId(Long id) 
	{
		this.id = id;
	}
	
	public int getPrimitive() 
	{
		return primitive;
	}

	public void setPrimitive(int primitive) 
	{
		this.primitive = primitive;
	}

	public LocalDateTime getDate() 
	{
		return date;
	}

	public void setDate(LocalDateTime date) 
	{
		this.date = date;
	}
	
	public Map<MutableInt, LocalDateTime>   getGenericsProperty()
    {
        return genericsProperty;
    }

    public void setGenericsProperty(Map<MutableInt, LocalDateTime> genericsProperty)
    {
        this.genericsProperty = genericsProperty;
    }

    public String getTransientProperty() 
	{
		return transientProperty;
	}

	public void setTransientProperty(String transientProperty) 
	{
		this.transientProperty = transientProperty;
	}

	public String getComputedProperty() 
	{
		return computedProperty;
	}

	@BuilderProperty
	public void setComputedProperty(String property)
	{
		this.computedProperty = property;
	}

	public String getDefaultStringProperty() 
	{
		return defaultStringProperty;
	}

	public void setDefaultStringProperty(String defaultStringProperty) 
	{
		this.defaultStringProperty = defaultStringProperty;
	}

	public String getDefaultStringProperty2() 
	{
		return defaultStringProperty2;
	}

	public void setDefaultStringProperty2(String defaultStringProperty2) 
	{
		this.defaultStringProperty2 = defaultStringProperty2;
	}

	public long getDefaultLongProperty() {
		return defaultLongProperty;
	}

	public void setDefaultLongProperty(long defaultLongProperty) 
	{
		this.defaultLongProperty = defaultLongProperty;
	}

	public char getDefaultCharProperty() 
	{
		return defaultCharProperty;
	}

	public void setDefaultCharProperty(char defaultCharProperty) 
	{
		this.defaultCharProperty = defaultCharProperty;
	}
}
