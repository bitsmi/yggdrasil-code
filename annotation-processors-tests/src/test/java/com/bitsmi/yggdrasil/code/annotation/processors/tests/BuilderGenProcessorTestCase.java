package com.bitsmi.yggdrasil.code.annotation.processors.tests;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.bitsmi.yggdrasil.commons.validation.ConstraintViolation;
import com.bitsmi.yggdrasil.commons.validation.IValidator;
import com.bitsmi.yggdrasil.commons.validation.Validation;
import com.bitsmi.yggdrasil.commons.validation.Validation.ConstraintStatementExecution;
import com.bitsmi.yggdrasil.commons.validation.exception.ConstraintViolationException;

public class BuilderGenProcessorTestCase 
{
	@Test
	@DisplayName("Simple builder test")
	public void builderTest()
	{
		SimpleTestEntity entity = SimpleTestEntityBuilder.newInstance()
				.withId(1L)
				.withPrimitive(10)
				.withDate(LocalDateTime.parse("2020-01-01T00:00:00.000", DateTimeFormatter.ISO_DATE_TIME))
				.withComputedProperty("computed value")
				.build();
		
		assertThat(entity.getId()).isEqualTo(1L);
		assertThat(entity.getPrimitive()).isEqualTo(10);
		assertThat(entity.getDate()).isEqualTo(LocalDateTime.parse("2020-01-01T00:00:00.000", DateTimeFormatter.ISO_DATE_TIME));
		assertThat(entity.getComputedProperty()).isEqualTo("computed value");
	}
	
	@Test
	@DisplayName("Builder with javax validation OK test")
	public void builderTest2()
	{
		ValidatedTestEntity entity = ValidatedTestEntityBuilder.newInstance()
				.withId(1L)
				.withPrimitive(10)
				.withEnabled(true)
				.withProperty("property value")
				.validated()
				.build();
		
		assertThat(entity.getId()).isEqualTo(1L);
		assertThat(entity.getPrimitive()).isEqualTo(10);
		assertThat(entity.isEnabled()).isTrue();
		assertThat(entity.getProperty()).isEqualTo("property value");
	}
	
	@Test
	@DisplayName("Builder with javax validation ERROR test")
	public void builderTest3()
	{
		Throwable t = catchThrowable(() -> {
			ValidatedTestEntityBuilder.newInstance()
					.withId(1L)
					// Validation error
					.withPrimitive(1)
					// Validation error
					.withEnabled(false)
					.withProperty("property value")
					.validated()
					.build();
		});
		
		assertThat(t).isInstanceOf(ConstraintViolationException.class);
		assertThat(((ConstraintViolationException)t).getViolations()).hasSize(2);
	}
	
	@Test
	@DisplayName("Builder with not enabled validation test")
	public void builderTest4()
	{
		ValidatedTestEntity entity = ValidatedTestEntityBuilder.newInstance()
				.withId(1L)
				// Wrong value
				.withPrimitive(1)
				// Wrong value
				.withEnabled(false)
				.withProperty("property value")
				// Validations will be skipped so the object will be built
				.build();
		
		assertThat(entity.getId()).isEqualTo(1L);
		assertThat(entity.getPrimitive()).isEqualTo(1);
		assertThat(entity.isEnabled()).isFalse();
		assertThat(entity.getProperty()).isEqualTo("property value");
	}
	
	@Test
	@DisplayName("Builder with simple validation test")
	public void builderTest5()
	{
		boolean[] validated = { false };
		ValidatedTestEntity entity = ValidatedTestEntityBuilder.newInstance()
				.withId(1L)
				// Wrong value
				.withPrimitive(1)
				// Wrong value
				.withEnabled(false)
				.withProperty("property value")
				.validated(new IValidator<ValidatedTestEntity>() 
				{
                    @Override
                    public Validation validate(ValidatedTestEntity instance)
                    {
                        validated[0] = true;
                        
                        Validation validation = Validation.validationWithDefaultExecutionGroup();
                        validation.getExecution()
                                .addStatementExecution(Validation.executedStatement("Validate test instance", 
                                    instance, 
                                    Validation.executedPlainConstraint("Is valid instance", instance)));
                        validation.getExecution().selectedGroup().success();
                        
                        validation.success();
                        
                        return validation;
                    }
				})
				.build();
		
		assertThat(validated[0]).isTrue();
		assertThat(entity.getId()).isEqualTo(1L);
		assertThat(entity.getPrimitive()).isEqualTo(1);
		assertThat(entity.isEnabled()).isFalse();
		assertThat(entity.getProperty()).isEqualTo("property value");
	}
	
	@Test
	@DisplayName("Builder with simple validation ERROR test")
	public void builderTest6()
	{
		boolean[] validated = { false };
		Throwable t = catchThrowable(() -> {
			ValidatedTestEntityBuilder.newInstance()
					.withId(1L)
					// Validation error
					.withPrimitive(1)
					// Validation error
					.withEnabled(false)
					.withProperty("property value")
					.validated(new IValidator<ValidatedTestEntity>() 
					{
					    @Override
	                    public Validation validate(ValidatedTestEntity instance)
	                    {
					        validated[0] = true;
					        
	                        Validation validation = Validation.validationWithDefaultExecutionGroup();
	                        
	                        validation.addConstraintViolation(new ConstraintViolation("Validate test instance 1", "Error 1"));
	                        validation.addConstraintViolation(new ConstraintViolation("Validate test instance 2", "Error 2"));
	                        validation.addConstraintViolation(new ConstraintViolation("Validate test instance 3", "Error 3"));
	                        
	                        ConstraintStatementExecution constraintExecution = Validation.executedStatement("Validate test instance", 
                                    instance, 
                                    Validation.executedPlainConstraint("Is valid instance", instance));
	                        constraintExecution.getConstraintExecution().fail();
	                        
	                        validation.getExecution()
	                                .addStatementExecution(constraintExecution);
	                        validation.getExecution().selectedGroup().fail();	                        
	                        validation.fail();
	                        
	                        return validation;
	                    }
					})
					.build();
		});
		
		assertThat(validated[0]).isTrue();
		assertThat(t).isInstanceOf(ConstraintViolationException.class);
		assertThat(((ConstraintViolationException)t).getViolations()).hasSize(3);
	}
	
	@Test
	@DisplayName("Custom named builder test")
	public void builderTest7()
	{
		NamedBuilderTestEntity entity = CustomEntityBuilder.newInstance()
				.withId(1L)
				.withPrimitive(10)
				.withDate(LocalDateTime.parse("2020-01-01T00:00:00.000", DateTimeFormatter.ISO_DATE_TIME))
				.withComputedProperty("computed value")
				.build();
		
		assertThat(entity.getId()).isEqualTo(1L);
		assertThat(entity.getPrimitive()).isEqualTo(10);
		assertThat(entity.getDate()).isEqualTo(LocalDateTime.parse("2020-01-01T00:00:00.000", DateTimeFormatter.ISO_DATE_TIME));
		assertThat(entity.getComputedProperty()).isEqualTo("computed value");
	}
	
	@Test
	@DisplayName("Simple builder default values test")
	public void builderTest8()
	{
		SimpleTestEntity entity = SimpleTestEntityBuilder.newInstance()
				.withId(1L)
				.withPrimitive(10)
				.withComputedProperty("computed value")
				.build();
		
		assertThat(entity.getId()).isEqualTo(1L);
		assertThat(entity.getPrimitive()).isEqualTo(10);
		assertThat(entity.getDate()).isEqualTo(LocalDateTime.parse("2021-01-01T12:00:00.000", DateTimeFormatter.ISO_DATE_TIME));
		assertThat(entity.getComputedProperty()).isEqualTo("computed value");
		assertThat(entity.getDefaultStringProperty()).isEqualTo("String default property");
		assertThat(entity.getDefaultStringProperty2()).isEqualTo("String default property 2");
		assertThat(entity.getDefaultLongProperty()).isEqualTo(3);
		assertThat(entity.getDefaultCharProperty()).isEqualTo('c');
	}
	
	@Test
    @DisplayName("Wildcard fields test")
    public void builderTest9()
    {
	    List<? super Exception> errors = new ArrayList<>();
	    errors.add(new RuntimeException());
	    errors.add(new IOException());
	    
	    GenericsWildcardTestEntity entity = GenericsWildcardTestEntityBuilder.newInstance()
	            .withErrors(errors)
	            .build();
	    
	    assertThat(entity.getErrors()).hasSize(2);
    }
	
	@Test
    @DisplayName("Generics class")
    public void builderTest10()
    {
	    GenericClassTestEntity entity = GenericClassTestEntityBuilder.newInstance()
            .withId(10L)
            .build();
    
	    assertThat(entity.getId()).isEqualTo(10L);
    }
}
