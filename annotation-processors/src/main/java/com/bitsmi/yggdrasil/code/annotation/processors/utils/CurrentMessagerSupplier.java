package com.bitsmi.yggdrasil.code.annotation.processors.utils;

import java.util.Optional;
import java.util.function.Supplier;

import javax.annotation.processing.Messager;

import com.bitsmi.yggdrasil.code.annotation.processors.AbstractLoggingProcessor;

public class CurrentMessagerSupplier implements Supplier<Optional<Messager>>
{
    @Override
    public Optional<Messager> get()
    {
        return AbstractLoggingProcessor.getCurrentMessager();
    }
}
