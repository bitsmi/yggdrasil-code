package com.bitsmi.yggdrasil.code.annotation.processors.utils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementScanner8;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ImportScanner extends ElementScanner8<Void, Void> 
{
    private final Logger log = LoggerFactory.getLogger(ImportScanner.class);
    
    private ProcessingEnvironment processingEnv;
    
    private boolean javaxValidationAvailable = false;
    
    public ImportScanner(ProcessingEnvironment processingEnv)
    {
        this.processingEnv = processingEnv;
    }
    
    private Set<String> types = new HashSet<>();

    public Set<String> getImportedTypes() 
    {
        return types;
    }

    @Override
    public Void visitType(TypeElement elem, Void p) 
    {
        for(TypeMirror interfaceType : elem.getInterfaces()) {
            processType((DeclaredType)interfaceType);
        }
        processType((DeclaredType)elem.getSuperclass());
        return super.visitType(elem, p);
    }

    @Override
    public Void visitExecutable(ExecutableElement elem, Void p) 
    {
        // Void and primitive return types are not processed
        if(elem.getReturnType().getKind()==TypeKind.DECLARED) {
            processType((DeclaredType)elem.getReturnType());
        }
        for(VariableElement paramElem:elem.getParameters()) {
            // Primitive parameters are not processed
            if(!paramElem.asType().getKind().isPrimitive()
                    // Generic E.G. T
                    && paramElem.asType().getKind()!=TypeKind.TYPEVAR) {
                processType((DeclaredType)paramElem.asType());
            }
        }
        inspectAnnotations(elem);
        
        return super.visitExecutable(elem, p);
    }

    @Override
    public Void visitTypeParameter(TypeParameterElement elem, Void p) 
    {
        if(elem.asType().getKind() == TypeKind.DECLARED) {
            processType((DeclaredType)elem.asType());
        }
        return super.visitTypeParameter(elem, p);
    }

    @Override
    public Void visitVariable(VariableElement elem, Void p) 
    {
        // Class or interface type
        if(elem.asType().getKind() == TypeKind.DECLARED) {
            processType((DeclaredType)elem.asType());
            inspectAnnotations(elem);
        }
        return super.visitVariable(elem, p);
    }
    
    public boolean isJavaxValidationAvailable()
    {
        return javaxValidationAvailable;
    }
    
    private void processType(DeclaredType type)
    {
        String typeStr = TypeUtils.getTypeCanonicalName(processingEnv, type);
        List<String> typeArgs = TypeUtils.getDeclaredTypeArgumentsCanonicalNames(processingEnv, type);
        
        log.info("Adding imported type: {}", typeStr);
        if(typeStr!=null && !typeStr.startsWith("java.lang")) {
            types.add(typeStr);
        }
        for(String typeArg:typeArgs) {
            log.info("Adding imported arg type: {}", typeArg);
            // java.lang classes don't need to be imported
            if(!typeArg.startsWith("java.lang")) {
                types.add(typeArg);
            }
        }
    }
    
    private void inspectAnnotations(Element elem)
    {
        for(AnnotationMirror annotation:elem.getAnnotationMirrors()) {
            String annotationClassName = TypeUtils.getTypeCanonicalName(processingEnv, annotation.getAnnotationType());
            log.info("Process type annotation: {}", annotationClassName);
            if(annotationClassName!=null && annotationClassName.startsWith("javax.validation")) {
                javaxValidationAvailable = true;
                break;
            }
        }
    }
}