package com.bitsmi.yggdrasil.code.annotation.processors;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.FIELD, ElementType.METHOD })
@Retention(RetentionPolicy.SOURCE)
public @interface BuilderProperty 
{
	public static final String NOT_DEFINED_STRING_VALUE = "__ND__";
	
	String defaultValue() default NOT_DEFINED_STRING_VALUE;
}