package com.bitsmi.yggdrasil.code.annotation.processors;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.WildcardType;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bitsmi.yggdrasil.code.annotation.processors.exception.TemplateException;
import com.bitsmi.yggdrasil.code.annotation.processors.utils.FreemarkerTemplateProcessor;
import com.bitsmi.yggdrasil.code.annotation.processors.utils.ImportScanner;
import com.bitsmi.yggdrasil.code.annotation.processors.utils.TypeUtils;
import com.google.auto.service.AutoService;

@SupportedAnnotationTypes("com.bitsmi.yggdrasil.code.annotation.processors.BuilderGen")
// populate META-INF/services/javax.annotation.processing.Processor
@AutoService(Processor.class)
public class BuilderGenProcessor extends AbstractLoggingProcessor
{	
	private static final String SRC_TEMPLATE = "/com/bitsmi/yggdrasil/code/annotation/processors/templates/builder.ftl";
	
	private final Logger log = LoggerFactory.getLogger(BuilderGenProcessor.class);
	
    @Override
	protected boolean doProcess(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv)
	{
	    log.info("Executing BuilderGenProcessor");	    
	    
		// Only "BuilderGen" annotation as specified in @SupportedAnnotationTypes
		for (TypeElement annotation : annotations) {
			Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(annotation);
			// BuilderGen is a type annotation, so elements are all classes
			for(Element elem:elements) {
			    log.info("processing element ({})", elem);
				BuilderClassModel model = new BuilderClassModel();
				collectBuilderClassData(model, (TypeElement)elem);
				
				try { 
				    createBuilder(model);
				}
    	        catch(TemplateException e) {
    	            log.error("Error generating builder source: {}", e.getMessage());
    	        }
    	        catch(IOException e) {
    	            log.error("Error generating writing builder class: {}", e.getMessage());
    	        }
			}
		}
		
		// Return "true" indicating that annotations were processed
		return true;
	}
	
	private void collectBuilderClassData(BuilderClassModel model, TypeElement elem)
	{
	    model.setBaseClassQualifiedName(elem.getQualifiedName().toString());
	    PackageElement packageElem = TypeUtils.getPackageElement(elem);
	    model.setBuilderClassPackage(packageElem.getQualifiedName().toString());
	    
	    List<?> typeParams = elem.getTypeParameters();
	    if(!typeParams.isEmpty()) {
	        List<String> typeVarNames = typeParams.stream()
	                .map(Object::toString)
	                .collect(Collectors.toList());
	        model.setBuilderClassTypeVars(typeVarNames);
	    }
	    
	    BuilderGen builderGenAnnotation = elem.getAnnotation(BuilderGen.class);        
	    if(!BuilderGen.NOT_DEFINED_STRING_VALUE.equals(builderGenAnnotation.builderName())) {
            model.setBuilderClassName(builderGenAnnotation.builderName());
        }
        else if(!BuilderGen.NOT_DEFINED_STRING_VALUE.equals(builderGenAnnotation.value())) {
            model.setBuilderClassName(builderGenAnnotation.value());
        }
        else {
            model.setBuilderClassName(model.getBaseClassName() + "Builder");
        }
	    
	    ImportScanner scanner = new ImportScanner(processingEnv);
        scanner.scan(Arrays.asList(elem), null);
     
        boolean javaxValidationAvailable = scanner.isJavaxValidationAvailable();
        log.info("Validation available: {}", elem);
        
        /* Imports */
        Set<String> importedTypes = new HashSet<>();
        // Default imports
        importedTypes = new TreeSet<>();
        importedTypes.add("java.util.List");
        importedTypes.add("java.util.Set");
        importedTypes.add("java.util.stream.Collectors");
        importedTypes.add("com.bitsmi.yggdrasil.commons.validation.IValidator");
        importedTypes.add("com.bitsmi.yggdrasil.commons.validation.exception.ConstraintViolationException");
        if(javaxValidationAvailable) {
            importedTypes.add("javax.validation.ConstraintViolation");
            importedTypes.add("javax.validation.Validation");
            importedTypes.add("javax.validation.Validator");
            importedTypes.add("javax.validation.ValidatorFactory");
        }
        importedTypes.addAll(scanner.getImportedTypes());
        
        model.setImports(importedTypes)
            .setJavaxValidationAvailable(javaxValidationAvailable);
        
        collectBuilderProperties(model, elem);
	}        
	
	private void collectBuilderProperties(BuilderClassModel model, TypeElement elem)
	{
		List<Element> propertyElements = elem.getEnclosedElements()
				.stream()
				.filter(prop -> TypeUtils.isElementAnnotated(prop, BuilderProperty.class))
				.collect(Collectors.toList());
		
		for(Element propElem:propertyElements) {
			// At this point elem is always annotated
			BuilderProperty annotation = propElem.getAnnotation(BuilderProperty.class);
			BuilderClassModel.BuilderPropertyModel propertyModel = new BuilderClassModel.BuilderPropertyModel()
					.setPropertyClass(computePropertyType(propElem))
					.setPrimitiveProperty(propElem.asType().getKind().isPrimitive())
					.setPropertyName(computePropertyName(propElem));
			
			if(!BuilderProperty.NOT_DEFINED_STRING_VALUE.equals(annotation.defaultValue())) {
				// Quote String values
				if("String".equals(propertyModel.getPropertyClass()) 
						&& annotation.defaultValue().startsWith("'")
						&& annotation.defaultValue().endsWith("'")) {
					// Trim simple quotes and replaced them with double quotes
					String value = annotation.defaultValue().substring(1, annotation.defaultValue().length()-1);
					propertyModel.setDefaultValue("\"" + value + "\"");
				}
				else if("String".equals(propertyModel.getPropertyClass())) {
					// String value is not quoted
					propertyModel.setDefaultValue("\"" + annotation.defaultValue() + "\"");
				}
				else if(("char".equals(propertyModel.getPropertyClass())
							|| "Character".equals(propertyModel.getPropertyClass()))
						&& !annotation.defaultValue().startsWith("'")
						&& !annotation.defaultValue().endsWith("'")) {
					propertyModel.setDefaultValue("'" + annotation.defaultValue() + "'");
				}
				else {
					propertyModel.setDefaultValue(annotation.defaultValue());
				}
			}
			
			model.addBuilderProperty(propertyModel);
		}
	}
	
	private String computePropertyType(Element elem)
	{
	    log.info("Computing property type: {}", elem);
	    
		// Primitive type
		if(elem.asType().getKind().isPrimitive()) {
			return TypeUtils.getPrimitiveTypeName((PrimitiveType)elem.asType());
		}
		// Generic E.G. T
		if(elem.asType().getKind()==TypeKind.TYPEVAR) {
		    return elem.asType().toString();
		}
		
		String type = null;
		// Method
		if(elem instanceof ExecutableElement) {
		    String name = elem.getSimpleName().toString();
			ExecutableElement methodInfo = (ExecutableElement)elem;
			// Look for setters
			if(methodInfo.getParameters().size()!=1 || !name.startsWith("set")) {
				processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
						String.format("Method is not a setter: %s", name), elem);
			}
			else {
				Element paramElement = methodInfo.getParameters().get(0);
				return computePropertyType(paramElement);
			}
		}
		// Variable element
		else if(elem instanceof VariableElement) {
		    DeclaredType declaredType = (DeclaredType)elem.asType();
		    // Element corresponding to field Class type E.G. java.lang.String
            String elemClassName =  TypeUtils.getTypeCanonicalName(processingEnv, declaredType);
            if(elemClassName==null) {
                elemClassName = declaredType.asElement().getSimpleName().toString();
            }
            
            List<String> argTypesClassNames = declaredType.getTypeArguments()
	                .stream()
	                .map(typeArg -> {
	                    String canonicalName = TypeUtils.getTypeCanonicalName(processingEnv, typeArg);
                        String result = StringUtils.substringAfterLast(canonicalName, ".");
	                    
	                    if(typeArg.getKind()==TypeKind.WILDCARD) {
	                        // Extends bounded wildcard
	                        if(TypeUtils.isExtendsBoundWildcardType((WildcardType)typeArg)) {
	                            result = "? extends " + result;
	                        }
	                        // Super bounded wildcard
	                        else {
	                            result = "? super " + result;
	                        }
	                    }	                    
	                    
	                    return result;
	                })
	                .collect(Collectors.toList());
	        StringBuilder resultBuilder = new StringBuilder(StringUtils.substringAfterLast(StringUtils.defaultString(elemClassName), "."));
	        if(!argTypesClassNames.isEmpty()) {
	            resultBuilder
	                .append("<")
	                .append(argTypesClassNames
	                    .stream()
	                    .collect(Collectors.joining(", ")))
	                .append(">");
	        }
	        type = resultBuilder.toString();	        
		}

		return type;
	}
	
	private String computePropertyName(Element elem)
	{
		String name = elem.getSimpleName().toString();
		// Method
		if(elem instanceof ExecutableElement) {
			if(!name.startsWith("set")) {
				processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
						String.format("Method is not a setter: %s", name), elem);
			}
			else {
				name = StringUtils.substringAfter(name, "set");
				name = StringUtils.uncapitalize(name);
			}
		}
		return name;
	}
	
	private void createBuilder(BuilderClassModel model) throws TemplateException, IOException
    {
        FreemarkerTemplateProcessor templateProcessor = new FreemarkerTemplateProcessor();
        String source = templateProcessor.processTemplate(SRC_TEMPLATE, model);
        
        JavaFileObject builderFile = processingEnv.getFiler()
                .createSourceFile(model.getBuilderClassPackage() + "." + model.getBuilderClassName());
        try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {
            out.println(source);
        }
    }
}
