package com.bitsmi.yggdrasil.code.annotation.processors.utils;

import org.slf4j.ILoggerFactory;

public class LoggerFactory implements ILoggerFactory
{
    private final CurrentMessagerSupplier messagerSupplier;

    public LoggerFactory()
    {
        messagerSupplier = new CurrentMessagerSupplier();
    }

    @Override
    public org.slf4j.Logger getLogger(String name)
    {
        return new Logger(new MessagePrinter(messagerSupplier, new MessageBuilder(name)));
    }
}
