package com.bitsmi.yggdrasil.code.annotation.processors;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.bitsmi.yggdrasil.commons.validation.IValidator;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface BuilderGen 
{
	public static abstract class NotDefinedValidatorClass implements IValidator<Void> { }
	
	public static final String NOT_DEFINED_STRING_VALUE = "__ND__";
	public static final Class<? extends IValidator<Void>> NOT_DEFINED_VALIDATOR_CLASS = NotDefinedValidatorClass.class;
	
	String builderName() default NOT_DEFINED_STRING_VALUE;
	String value() default NOT_DEFINED_STRING_VALUE;
	Class<? extends IValidator<?>> simpleValidator() default NotDefinedValidatorClass.class;
}
