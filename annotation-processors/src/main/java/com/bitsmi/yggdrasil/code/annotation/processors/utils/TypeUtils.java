package com.bitsmi.yggdrasil.code.annotation.processors.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.ReferenceType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import javax.lang.model.type.WildcardType;

public class TypeUtils 
{
    public static PackageElement getPackageElement(TypeElement elem)
    {
        Element currentElement = elem;
        while(currentElement!=null && currentElement.getKind()!=ElementKind.PACKAGE) {
            currentElement = currentElement.getEnclosingElement();
        }
        
        if(currentElement!=null && currentElement.getKind()==ElementKind.PACKAGE) {
            return (PackageElement) currentElement;
        }
        return null;
    }
    
    public static String getTypeCanonicalName(ProcessingEnvironment processingEnv, TypeMirror type)
    {
        Objects.requireNonNull(processingEnv);
        Objects.requireNonNull(type);
        
        String result = null;
        if(type.getKind().isPrimitive()) {
            PrimitiveType paramType = (PrimitiveType)type;
            result = getPrimitiveTypeName(paramType);
        }
        else if(type.getKind()==TypeKind.ARRAY) {
            ArrayType paramType = (ArrayType)type;
            return getTypeCanonicalName(processingEnv, paramType.getComponentType());
        }
        else if(type.getKind()==TypeKind.WILDCARD) {
            WildcardType wildcardType = (WildcardType)type;
            TypeMirror extendsBound = wildcardType.getExtendsBound();
            // First check extends bound
            if(extendsBound!=null) {
                return getTypeCanonicalName(processingEnv, extendsBound);
            }
            // If no extends bound its a super bound
            else {
                TypeMirror superBound = wildcardType.getSuperBound();
                return getTypeCanonicalName(processingEnv, superBound);
            }
        }
        // Type variables
        else if(type.getKind()==TypeKind.TYPEVAR) {
            TypeVariable typeVariable = (TypeVariable)type;
            TypeParameterElement typeVariableElement = (TypeParameterElement)processingEnv.getTypeUtils().asElement(typeVariable);

            // T, R...
            if(typeVariableElement.getKind()==ElementKind.TYPE_PARAMETER) {
                return null;
            }
            
            Element currentElement = typeVariableElement;
            result = typeVariableElement.getSimpleName().toString();
            while (currentElement.getEnclosingElement()!=null) {
                currentElement = currentElement.getEnclosingElement();
                if (currentElement.asType().getKind()==TypeKind.DECLARED) {
                    result = currentElement.getSimpleName() + "$" + result;
                } 
                else if (currentElement.asType().getKind()==TypeKind.PACKAGE) {
                    result = ((PackageElement) currentElement).getQualifiedName() + "." + result;
                }
            }
        }
        else {
            ReferenceType referenceType = (ReferenceType)type;
            // Element corresponding to param Class type E.G. java.lang.String
            TypeElement referenceTypeElement = (TypeElement)processingEnv.getTypeUtils().asElement(referenceType);
            
            Element currentElement = referenceTypeElement;
            result = referenceTypeElement.getSimpleName().toString();
            while (currentElement.getEnclosingElement()!=null) {
                currentElement = currentElement.getEnclosingElement();
                if (currentElement.asType().getKind()==TypeKind.DECLARED) {
                    result = currentElement.getSimpleName() + "$" + result;
                } 
                else if (currentElement.asType().getKind()==TypeKind.PACKAGE) {
                    result = ((PackageElement) currentElement).getQualifiedName() + "." + result;
                }
            }
        }
        
        return result;
    }
    
    public static List<String> getDeclaredTypeArgumentsCanonicalNames(ProcessingEnvironment processingEnv, DeclaredType type)
    {
        Objects.requireNonNull(processingEnv);
        Objects.requireNonNull(type);
        
        if(type.getKind().isPrimitive()
                || type.getKind()==TypeKind.ARRAY) {
            return Collections.emptyList();
        }
        
        List<String> results = new ArrayList<>(); 
        List<? extends TypeMirror> typeArgs = type.getTypeArguments();
        for(int i=0; i<typeArgs.size(); i++) {
            TypeMirror typeArg = typeArgs.get(i);
            String typeStr = getTypeCanonicalName(processingEnv, typeArg);
            if(typeStr!=null) {
                results.add(typeStr);
            }
        }
        
        return results;
    }
    
    public static String getPrimitiveTypeName(PrimitiveType type)
    {
        switch(type.getKind()) {
            case BOOLEAN:
                return "boolean";
            case CHAR:
                return "char";
            case BYTE:
                return "byte";
            case SHORT:
                return "short";
            case INT:
                return "int";
            case LONG:
                return "long";
            case FLOAT:
                return "float";
            case DOUBLE:
                return "double";
            default:
                throw new IllegalArgumentException("Non primitive type " + type.toString());
        }
    }
    
	public static boolean isElementAnnotated(Element element, Class<?> annotionClass)
	{
	    Objects.requireNonNull(annotionClass);
	    
	    String clazzName = annotionClass.getName();
	    for(AnnotationMirror m : element.getAnnotationMirrors()) {
	        if(m.getAnnotationType().toString().equals(clazzName)) {
	            return true;
	        }
	    }
	    return false;
	}
	
	public static boolean isSuperBoundWildcardType(WildcardType type)
	{
	    return type.getSuperBound()!=null;
	}
	
	public static boolean isExtendsBoundWildcardType(WildcardType type)
    {
        return type.getExtendsBound()!=null;
    }
}

