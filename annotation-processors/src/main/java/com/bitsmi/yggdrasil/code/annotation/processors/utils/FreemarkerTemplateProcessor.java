package com.bitsmi.yggdrasil.code.annotation.processors.utils;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

import com.bitsmi.yggdrasil.code.annotation.processors.exception.TemplateException;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;

public class FreemarkerTemplateProcessor 
{
    private Configuration configuration;
    
    public FreemarkerTemplateProcessor()
    {
        /* Create Configuration instance and specify up to what FreeMarker
         * version (here 2.3.30) do you want to apply the fixes that are not 100%
         * backward compatible. See Configuration JavaDoc for details.
         */
        configuration = new Configuration(Configuration.VERSION_2_3_30);
        configuration.setClassForTemplateLoading(this.getClass(), "/");
        configuration.setDefaultEncoding(StandardCharsets.UTF_8.name());
        configuration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        configuration.setLogTemplateExceptions(true);
    }
    
    public Template resolveTemplate(String template) throws IOException
    {
        Objects.requireNonNull(template);
        
        if(template.startsWith("/")) {
            template = template.substring(1);
        }
        
        return configuration.getTemplate(template);
    }
    
    public String processTemplate(String template, Object model) throws TemplateException
    {
        try {
            StringWriter sw = new StringWriter();
            resolveTemplate(template).process(model, sw);
            String processedText = sw.toString();
            
            return processedText;
        }
        catch(IOException | freemarker.template.TemplateException e) {
            throw new TemplateException(String.format("Error processing template (%s)", template), e);
        }
    }
}

