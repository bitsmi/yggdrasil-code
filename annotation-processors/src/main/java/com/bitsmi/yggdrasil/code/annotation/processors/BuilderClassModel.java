package com.bitsmi.yggdrasil.code.annotation.processors;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

public class BuilderClassModel 
{
	private String builderClassPackage;
	private String builderClassName;
	// E.G. GenericClassTestEntityBuilder<T>
	private List<String> builderClassTypeVars = new ArrayList<>();
	private Set<String> imports;
	private String baseClassQualifiedName;
	private List<BuilderPropertyModel> builderProperties = new ArrayList<>();
	private boolean javaxValidationAvailable = false;
	
	public String getBuilderClassPackage() 
	{
		return builderClassPackage;
	}
	
	public BuilderClassModel setBuilderClassPackage(String builderClassPackage) 
	{
		this.builderClassPackage = builderClassPackage;
		return this;
	}
	
	public String getBuilderClassName()
	{
		return builderClassName;
	}
	
	/**
	 * Returns the builder class name along the necessary type vars E.G. GenericClassEntityBuilder<T>.
	 * If the base class has not type variables, the returned value will be the same as <b>getBuilderClassName()</b>
	 * @return
	 */
	public String getBuilderClassNameWithTypeVars()
    {
	    StringBuilder nameBuilder = new StringBuilder(getBuilderClassName());
	    if(hasBuilderClassTypeVars()) {
	        nameBuilder.append("<")
	                .append(getBuilderClassTypeVarsAsString())
	                .append(">");
	    }
	    
        return nameBuilder.toString();
    }
	
	public BuilderClassModel setBuilderClassName(String builderClassName)
	{
		this.builderClassName = builderClassName;
		return this;
	}
	
	public List<String> getBuilderClassTypeVars()
    {
        return builderClassTypeVars;
    }

    public BuilderClassModel setBuilderClassTypeVars(List<String> builderClassTypeVars)
    {
        this.builderClassTypeVars = builderClassTypeVars;
        return this;
    }
    
    public BuilderClassModel addBuilderClassTypeVar(String builderClassTypeVar)
    {
        this.builderClassTypeVars.add(builderClassTypeVar);
        return this;
    }
    
    public boolean hasBuilderClassTypeVars() 
    {
        return !builderClassTypeVars.isEmpty();
    }
    
    public String getBuilderClassTypeVarsAsString()
    {
        return builderClassTypeVars.stream()
            .collect(Collectors.joining(","));
    }

    public Set<String> getImports() 
	{
		return imports;
	}

	public BuilderClassModel setImports(Set<String> imports) 
	{
		this.imports = imports;
		return this;
	}

	public String getBaseClassQualifiedName()
	{
		return baseClassQualifiedName;
	}
	
	public BuilderClassModel setBaseClassQualifiedName(String baseClassQualifiedName) 
	{
		this.baseClassQualifiedName = baseClassQualifiedName;
		return this;
	}
	
	public String getBaseClassName()
	{
		return baseClassQualifiedName.substring(baseClassQualifiedName.lastIndexOf(".")+1, baseClassQualifiedName.length());
	}
	
	/**
     * Returns the builder class name along the necessary type vars E.G. GenericClassEntity<T>.
     * If the base class has not type variables, the returned value will be the same as <b>getBaseClassName()</b>
     * @return
     */
    public String getBaseClassNameWithTypeVars()
    {
        StringBuilder nameBuilder = new StringBuilder(getBaseClassName());
        if(hasBuilderClassTypeVars()) {
            nameBuilder.append("<")
                    .append(getBuilderClassTypeVarsAsString())
                    .append(">");
        }
        
        return nameBuilder.toString();
    }
	
	public List<BuilderPropertyModel> getBuilderProperties() 
	{
		return builderProperties;
	}

	public BuilderClassModel setBuilderProperties(List<BuilderPropertyModel> builderProperties) 
	{
		this.builderProperties = builderProperties;
		return this;
	}

	public BuilderClassModel addBuilderProperty(BuilderPropertyModel builderProperty) 
	{
		this.builderProperties.add(builderProperty);
		return this;
	}
	
	public boolean isJavaxValidationAvailable() 
	{
		return javaxValidationAvailable;
	}

	public BuilderClassModel setJavaxValidationAvailable(boolean javaxValidationAvailable) 
	{
		this.javaxValidationAvailable = javaxValidationAvailable;
		return this;
	}

	public static class BuilderPropertyModel
	{
		private String propertyClass;
		private boolean primitiveProperty;
		private String propertyName;
		private boolean defaultValueAvailable = false;
		private String defaultValue;
		
		public String getPropertyClass() 
		{
			return propertyClass;
		}
		
		public String getPrimitivePropertyBoxedClass()
		{
			String boxedClass = null;
			switch(propertyClass) {
				case "boolean":
					boxedClass = "Boolean";
					break;
				case "byte":
					boxedClass = "Byte";
					break;
				case "char":
					boxedClass = "Character";
					break;
				case "short": 
					boxedClass = "Short"; 
					break;
				case "int": 
					boxedClass = "Integer"; 
					break; 
				case "long": 
					boxedClass = "Long"; 
					break;
				case "float":
					boxedClass = "Float";
					break;
				case "double": 
					boxedClass = "Double"; 
					break;
				default:
				    boxedClass = propertyClass;
					break;
			}

			return boxedClass;
		}
		
		public BuilderPropertyModel setPropertyClass(String propertyClass) 
		{
			this.propertyClass = propertyClass;
			return this;
		}
		
		public boolean getPrimitiveProperty()
		{
			return primitiveProperty;
		}
		
		public BuilderPropertyModel setPrimitiveProperty(boolean primitiveProperty)
		{
			this.primitiveProperty = primitiveProperty;
			return this;
		}
		
		public String getPropertyName() 
		{
			return propertyName;
		}
		
		public BuilderPropertyModel setPropertyName(String propertyName) 
		{
			this.propertyName = propertyName;
			return this;
		}
		
		public String getDefaultValue()
		{
			return defaultValue;
		}
		
		public BuilderPropertyModel setDefaultValue(String defaultValueExpr)
		{
			this.defaultValue = defaultValueExpr;
			this.defaultValueAvailable = true;
			return this;
		}
		
		public boolean getDefaultValueAvailable()
		{
			return defaultValueAvailable;
		}
		
		public String getSetterMethodName()
		{
			return "set" + StringUtils.capitalize(propertyName);
		}
		
		public String getBuilderMethodName()
		{
			return "with" + StringUtils.capitalize(propertyName);
		}
	}
}
