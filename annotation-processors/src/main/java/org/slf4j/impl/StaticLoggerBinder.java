package org.slf4j.impl;

import org.slf4j.ILoggerFactory;
import org.slf4j.spi.LoggerFactoryBinder;

import com.bitsmi.yggdrasil.code.annotation.processors.utils.LoggerFactory;

public final class StaticLoggerBinder implements LoggerFactoryBinder 
{
    public static final StaticLoggerBinder SINGLETON = new StaticLoggerBinder();

    /**
     * Declare the version of the SLF4J API this implementation is compiled against.
     * The value of this field is usually modified with each release.
     */
    // to avoid constant folding by the compiler, this field must *not* be final
    public static String REQUESTED_API_VERSION = "1.7.25";

    public ILoggerFactory getLoggerFactory() 
    {
        return new LoggerFactory();
    }

    public String getLoggerFactoryClassStr() 
    {
        return LoggerFactory.class.getName();
    }

    public static StaticLoggerBinder getSingleton() {
        return SINGLETON;
    }
}
