package ${builderClassPackage};

<#list imports as elem>
import ${elem};
</#list>	

import ${baseClassQualifiedName};

public class ${builderClassNameWithTypeVars}
{
	<#list builderProperties as elem>
	<#if !elem.primitiveProperty>
	protected ${elem.propertyClass} ${elem.propertyName};
	<#else>
	protected ${elem.primitivePropertyBoxedClass} ${elem.propertyName};
	</#if>
	</#list>
	
	private IValidator<${baseClassNameWithTypeVars}> simpleValidator;
	<#if javaxValidationAvailable>
	private Validator javaxValidator = Validation.buildDefaultValidatorFactory().getValidator(); 
	</#if>
	private boolean validated = false;
    
    private ${builderClassName}()
    {
    
    }
    
    public static <#if hasBuilderClassTypeVars()><${getBuilderClassTypeVarsAsString()}></#if> ${builderClassNameWithTypeVars} newInstance()
    {
        return new ${builderClassNameWithTypeVars}();
    }
    
	<#list builderProperties as elem>
	public ${builderClassNameWithTypeVars} ${elem.builderMethodName}(${elem.propertyClass} value)
	{
		this.${elem.propertyName} = value;
		return this;
	}
	<#if !elem.primitiveProperty>
	public ${builderClassNameWithTypeVars} ${elem.builderMethodName}(${elem.propertyClass} value, ${elem.propertyClass} defaultValue)
	{
		if(value==null) {
			this.${elem.propertyName} = defaultValue;
		}
		else{
			this.${elem.propertyName} = value;
		}
		
		return this;
	}
	</#if>
	</#list>

	<#if javaxValidationAvailable>
	public ${builderClassNameWithTypeVars} validated()
	{
		this.validated = true;
		return this;
	}
	
	public ${builderClassNameWithTypeVars} validated(Validator validator)
	{
		this.javaxValidator = validator;
		this.validated = true;
		return this;
	}
	
	private void performJavaxValidation(${baseClassNameWithTypeVars} instance)
	{
		Set<ConstraintViolation<${baseClassNameWithTypeVars}>> violations = javaxValidator.validate(instance);
		if(!violations.isEmpty()) {
			List<com.bitsmi.yggdrasil.commons.validation.ConstraintViolation> errors = violations.stream()
					.map(v -> new com.bitsmi.yggdrasil.commons.validation.ConstraintViolation(
					        v.getConstraintDescriptor().getAnnotation().annotationType().getName(),
					        v.getMessage()))
					.collect(Collectors.toList());
			
			throw new ConstraintViolationException("Invalid ${baseClassName}", errors);
		}		
	}
	</#if>
	
	public ${builderClassNameWithTypeVars} validated(IValidator<${baseClassNameWithTypeVars}> validator)
	{
		this.simpleValidator = validator;
		this.validated = true;
		return this;
	}
	
	private void performSimpleValidation(${baseClassNameWithTypeVars} instance)
	{
		com.bitsmi.yggdrasil.commons.validation.Validation validation = simpleValidator.validate(instance);
	    if(!validation.isSuccess()) {
	        List<com.bitsmi.yggdrasil.commons.validation.ConstraintViolation> errors = validation.getConstraintViolations();
	        throw new ConstraintViolationException("Invalid NamedBuilderTestEntity", errors);
	    }		
	}
	
	public ${baseClassNameWithTypeVars} build()
	{
		${baseClassNameWithTypeVars} instance = new ${baseClassNameWithTypeVars}();
		<#list builderProperties as elem>
		<#if elem.defaultValueAvailable>
		instance.${elem.setterMethodName}(this.${elem.propertyName}!=null 
				? this.${elem.propertyName}
				: ${elem.defaultValue});
		<#else>
		instance.${elem.setterMethodName}(this.${elem.propertyName});
		</#if>
		</#list>
		
		if(validated && simpleValidator!=null) {
			this.performSimpleValidation(instance);
		}
		<#if javaxValidationAvailable>
		else if(validated && javaxValidator!=null) {
			this.performJavaxValidation(instance);
		}
		</#if>
		
		return instance;
	}
}